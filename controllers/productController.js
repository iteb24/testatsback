const express = require ('express');
var router = express.Router();
const mongoose = require('mongoose');

var Product = require('../models/product.model.js');


router.get('/test',(req,res)=>{

    res.json('sample test');
});



router.post("/",(req,res)=>{
    var product = new Product(req.body)
    product.save((err,product)=>{
        if(err) res.json(err);
        else res.json(product);
    })
});

router.get("/products",(req,res)=>{
    Product.find((err,products)=>{
        if(err) res.json(err)
        else res.json(products)
    })
});
router.get("/product/:id",(req,res)=>{
    Product.findById(req.params.id,(err,products)=>{
        if(err) res.json(err)
        else res.json(products)
    })
});

module.exports = router;