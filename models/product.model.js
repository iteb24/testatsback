const mongoose = require('mongoose');

var review = new mongoose.Schema({
    "rating" : {type : Number},
    "content" : {type : String},
},{ _id : false });

var productSchema = new mongoose.Schema({

    color: {type: String},
    category: {type: String},
    productName: {type: String},
    price: {type: Number},
    description: {type: String},
    tag: {type: String},
    productMaterial: {type: String},
    imageUrl: {type: String},
    createdAt: {type: Date},
    reviews: [review]
});

module.exports = mongoose.model('Product',productSchema);