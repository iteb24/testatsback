require ('./models/db');

const express = require('express');
const path = require('path');
const exphbs= require('express-handlebars');
var Product = require('./models/product.model.js');
var bodyParser  = require('body-parser');

var app= express();

app.set('views',path.join(__dirname,'/views/'));
app.engine('hbs',exphbs({extname :'hbs', defaultLayout:'mainLayout',layoutsDir:__dirname +'/views/layouts/' }));
app.set('view engine','hbs');

const productController =require('./controllers/productController');

// Middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));




app.use('/',productController);

app.listen(3000,() =>{
    console.log("Server is listening on port 3000");

});
